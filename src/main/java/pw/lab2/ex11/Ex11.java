package pw.lab2.ex11;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ex11 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        String inputFile = args[0];
        var countLetters = new CountLetters();
        countLetters.run(inputFile);
    }

    static class CountLetters {

        public void run(String inputFile) throws URISyntaxException, IOException {
            URL resource = Optional.ofNullable(getClass().getClassLoader()
                .getResource(inputFile)).orElseThrow(() -> new IOException("File not found!"));
            Path path = Paths.get(resource.toURI());
            try (Stream<String> lines = Files.lines(path)) {
                Map<Character, Integer> characterIntegerMap = lines.flatMap(s -> Arrays.stream(s.split("")))
                    .map(s -> s.charAt(0))
                    .filter(Character::isLetter)
                    .collect(Collectors.toMap(c -> c, c -> 1, Integer::sum));
                Integer count = characterIntegerMap.values().stream().reduce(Integer::sum)
                    .orElseThrow(() -> new RuntimeException("Error while calculating letters count"));
                characterIntegerMap.forEach((key, value) -> System.out.printf("%c = %.1f%% %n", key, (100d* value / count)));
            }
        }
    }
}
