package pw.lab2.ex5;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Ex5 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        String inputFile = args[0];
        var reverseTheFile = new ReverseTheFile();
        System.out.println("Reversed by char: ");
        reverseTheFile.charByChar(inputFile);
        System.out.println("\nReversed by line: ");
        reverseTheFile.lineByLine(inputFile);
    }

    static class ReverseTheFile {

        public void charByChar(String inputFile) throws URISyntaxException, IOException {
            File file = getFile(inputFile);
            try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r")) {
                long fileLength = file.length() - 1;
                randomAccessFile.seek(fileLength);
                for (long pointer = fileLength; pointer >= 0; pointer--) {
                    randomAccessFile.seek(pointer);
                    char c;
                    c = (char) randomAccessFile.read();
                    System.out.print(c);
                }
            }
        }

        public void lineByLine(String inputFile) throws URISyntaxException, IOException {
            List<String> lines = new ArrayList<>();
            File file = getFile(inputFile);
            long fileLength = file.length() - 1;
            StringBuilder builder = new StringBuilder();
            try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r")) {
                randomAccessFile.seek(fileLength);
                for (long pointer = fileLength; pointer >= 0; pointer--) {
                    randomAccessFile.seek(pointer);
                    char c;
                    c = (char) randomAccessFile.read();
                    if (c == '\n') {
                        lines.add(builder.reverse().toString());
                        builder = new StringBuilder();
                        continue;
                    }
                    builder.append(c);
                }
                lines.add(builder.reverse().toString());
                lines.forEach(System.out::println);
            }
        }

        private File getFile(String inputFile) throws IOException, URISyntaxException {
            URL resource = Optional.ofNullable(getClass().getClassLoader()
                .getResource(inputFile)).orElseThrow(() -> new IOException("File not found!"));
            Path path = Paths.get(resource.toURI());
            return path.toFile();
        }
    }
}
