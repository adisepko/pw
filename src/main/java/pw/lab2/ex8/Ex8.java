package pw.lab2.ex8;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Stream;

public class Ex8 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        var toUpperCase = new ToUpperCase();
        if (args.length == 0) {
            toUpperCase.readFromStdin();
        } else {
            String inputFile = args[0];
            toUpperCase.readFromFile(inputFile);
        }
    }

    static class ToUpperCase {

        public void readFromFile(String inputFile) throws URISyntaxException, IOException {
            URL resource = Optional.ofNullable(getClass().getClassLoader()
                .getResource(inputFile)).orElseThrow(() -> new IOException("File not found!"));
            Path path = Paths.get(resource.toURI());
            try (Stream<String> lines = Files.lines(path)) {
                lines.map(String::toUpperCase).forEach(System.out::println);
            }
        }

        public void readFromStdin() {
            System.out.println("Enter string:");
            Scanner scanner = new Scanner(System.in);
            String s = scanner.nextLine();
            System.out.println(s.toUpperCase());
        }
    }
}
