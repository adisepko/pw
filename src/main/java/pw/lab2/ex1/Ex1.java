package pw.lab2.ex1;

import io.vavr.control.Try;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Stream;

public class Ex1 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        String inputFile = args[0];
        String outputFile = args[1];
        CopyFileTask copyFileTask = new CopyFileTask();
        copyFileTask.run(inputFile, outputFile);
    }

    static class CopyFileTask {

        public void run(String inputFile, String outputFile) throws URISyntaxException, IOException {
            URL resource = Optional.ofNullable(getClass().getClassLoader()
                .getResource(inputFile)).orElseThrow(() -> new IOException("File not found!"));
            Path path = Paths.get(resource.toURI());
            try (Stream<String> lines = Files.lines(path);
                RandomAccessFile stream = new RandomAccessFile(new File(path.getParent().toFile(), outputFile), "rw");
                FileChannel channel = stream.getChannel()) {
                FileLock lock = channel.tryLock();
                lines.forEach(s -> Try.run(() -> stream.writeBytes(s+"\n")).get());
                lock.release();
            }
        }
    }
}
