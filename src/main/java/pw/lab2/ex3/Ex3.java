package pw.lab2.ex3;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

public class Ex3 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        String inputFile = args[0];
        FindLongestLineTask findLongestLineTask = new FindLongestLineTask();
        findLongestLineTask.run(inputFile);
    }

    static class FindLongestLineTask {

        public void run(String inputFile) throws URISyntaxException, IOException {
            URL resource = Optional.ofNullable(getClass().getClassLoader()
                .getResource(inputFile)).orElseThrow(() -> new IOException("File not found!"));
            Path path = Paths.get(resource.toURI());
            try (Stream<String> lines = Files.lines(path)) {
                String longestLine = lines.max(Comparator.comparingInt(String::length))
                    .orElseThrow(() -> new RuntimeException("Could not find longest line!"));
                System.out.printf("Longest line is \"%s\" and has: %d chars", longestLine, longestLine.length());
            }
        }
    }
}
