package pw.lab1.ex4;


public class Ex4 {

    private static final int QUEUE_SIZE = 10;

    public static void main(String[] args) throws InterruptedException {
        DataQueue dataQueue = new DataQueue(QUEUE_SIZE);

        Producer producer = new Producer(dataQueue);
        Thread producerThread = new Thread(producer);

        Consumer consumer = new Consumer(dataQueue);
        Thread consumerThread = new Thread(consumer);

        producerThread.start();
        consumerThread.start();

        Thread.sleep(5000);

        producer.stop();
        while (!dataQueue.isEmpty()){
            Thread.sleep(1000);
        }
        consumer.stop();

        producerThread.join();
        consumerThread.join();
    }
}