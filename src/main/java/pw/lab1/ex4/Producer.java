package pw.lab1.ex4;

import io.vavr.control.Try;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Producer implements Runnable {

    private final DataQueue dataQueue;
    private volatile boolean runFlag;

    private static int idSequence = 0;

    public Producer(DataQueue dataQueue) {
        this.dataQueue = dataQueue;
        runFlag = true;
    }

    @Override
    public void run() {
        Try.run(this::produce).get();
    }

    public void produce() throws InterruptedException {
        while (runFlag) {
            Message message = generateMessage();
            while (dataQueue.isFull() && runFlag) {
                try {
                    dataQueue.waitOnFull();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
            if (!runFlag) {
                break;
            }

            System.out.printf("ProducerThread: %s, message id: %d, data: %s%n", Thread.currentThread().getId(), message.id(),
                message.data().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            dataQueue.add(message);
            dataQueue.notifyAllForEmpty();
            Thread.sleep(100);
        }
        System.out.println("Producer Stopped");
    }

    private Message generateMessage() {
        return new Message(++idSequence, LocalDateTime.now());
    }

    public void stop() {
        runFlag = false;
        dataQueue.notifyAllForFull();
    }
}
