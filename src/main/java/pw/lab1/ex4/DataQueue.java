package pw.lab1.ex4;

import java.util.LinkedList;
import java.util.Queue;

public class DataQueue {

    private final Queue<Message> queue = new LinkedList<>();
    private final int maxSize;
    private final Object FULL = new Object();
    private final Object EMPTY = new Object();

    DataQueue(int maxSize) {
        this.maxSize = maxSize;
    }

    public boolean isFull() {
        return queue.size() == maxSize;
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public void waitOnFull() throws InterruptedException {
        synchronized (FULL) {
            FULL.wait();
        }
    }

    public void waitOnEmpty() throws InterruptedException {
        synchronized (EMPTY) {
            EMPTY.wait();
        }
    }

    public void notifyAllForFull() {
        synchronized (FULL) {
            FULL.notifyAll();
        }
    }

    public void notifyAllForEmpty() {
        synchronized (EMPTY) {
            EMPTY.notifyAll();
        }
    }

    public void add(Message message) {
        synchronized (queue) {
            queue.add(message);
        }
    }

    public Message remove() {
        synchronized (queue) {
            return queue.poll();
        }
    }
}