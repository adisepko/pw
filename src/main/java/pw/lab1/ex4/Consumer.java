package pw.lab1.ex4;

import io.vavr.control.Try;
import java.time.format.DateTimeFormatter;

public class Consumer implements Runnable {

    private final DataQueue dataQueue;
    private volatile boolean runFlag;

    public Consumer(DataQueue dataQueue) {
        this.dataQueue = dataQueue;
        runFlag = true;
    }

    @Override
    public void run() {
        Try.run(this::consume).get();
    }

    public void consume() throws InterruptedException {
        while (runFlag) {
            Message message;
            if (dataQueue.isEmpty()) {
                try {
                    dataQueue.waitOnEmpty();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
            if (!runFlag) {
                break;
            }
            message = dataQueue.remove();
            dataQueue.notifyAllForFull();
            useMessage(message);
        }
        System.out.println("Consumer Stopped");
    }

    private void useMessage(Message message) throws InterruptedException {
        if (message != null) {
            System.out.printf("ConsumerThread: %s, message id: %d, data: %s%n", Thread.currentThread().getId(), message.id(),
                message.data().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        }
        Thread.sleep(1000);

    }

    public void stop() {
        runFlag = false;
        dataQueue.notifyAllForEmpty();
    }
}
