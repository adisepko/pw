package pw.lab1.ex4;

import java.time.LocalDateTime;

public record Message(int id, LocalDateTime data) {

}