package pw.lab1.ex5;

import io.vavr.control.Try;
import java.time.format.DateTimeFormatter;

public class Reader implements Runnable {

    private final BoundedBuffer buffer;
    private volatile boolean runFlag;

    public Reader(BoundedBuffer buffer) {
        this.buffer = buffer;
        runFlag = true;
    }

    @Override
    public void run() {
        Try.run(this::read).get();
    }

    public void read() throws InterruptedException {
        while (runFlag) {
            if(buffer.count!=0){
                var message = buffer.take();
                System.out.printf("ReaderThread: %s, message id: %d, data: %s%n", Thread.currentThread().getId(), message.id(),
                    message.data().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            }
            Thread.sleep(1000);
        }
        System.out.println("Reader Stopped");
    }

    public void stop() {
        runFlag = false;
    }
}
