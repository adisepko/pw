package pw.lab1.ex5;

import io.vavr.control.Try;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Ex5 {

    public static void main(String[] args) throws InterruptedException {
        BoundedBuffer boundedBuffer = new BoundedBuffer();
        List<Writer> writers = getStreamOfThreads(5, () -> new Writer(boundedBuffer)).map(Writer.class::cast).toList();
        List<Reader> readers = getStreamOfThreads(3, () -> new Reader(boundedBuffer)).map(Reader.class::cast).toList();
        List<Thread> writerThreads = writers.stream().map(Thread::new).toList();
        List<Thread> readerThreads = readers.stream().map(Thread::new).toList();
        writerThreads.forEach(Thread::start);
        readerThreads.forEach(Thread::start);

        Thread.sleep(1900);

        writers.forEach(Writer::stop);
        while (boundedBuffer.count != 0) {
            Thread.sleep(1000);
        }
        readers.forEach(Reader::stop);
        writerThreads.forEach(t -> Try.run(t::join).get());
        readerThreads.forEach(t -> Try.run(t::join).get());
    }

    private static Stream<Runnable> getStreamOfThreads(int streamSize, Supplier<Runnable> runnableSupplier) {
        Stream.Builder<Runnable> writerThreads = Stream.builder();
        for (int i = 0; i < streamSize; i++) {
            writerThreads.add(runnableSupplier.get());
        }
        return writerThreads.build();
    }
}
