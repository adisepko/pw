package pw.lab1.ex5;

import java.time.LocalDateTime;

public record Message(int id, LocalDateTime data) {

}