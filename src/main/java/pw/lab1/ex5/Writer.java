package pw.lab1.ex5;

import io.vavr.control.Try;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Writer implements Runnable {

    private final BoundedBuffer buffer;
    private volatile boolean runFlag;
    private static int idSequence = 0;

    public Writer(BoundedBuffer buffer) {
        this.buffer = buffer;
        runFlag = true;
    }

    @Override
    public void run() {
        Try.run(this::read).get();
    }

    public void read() throws InterruptedException {
        while (runFlag) {
            var message = new Message(++idSequence, LocalDateTime.now());
            System.out.printf("WriterThread: %s, message id: %d, data: %s%n", Thread.currentThread().getId(), message.id(),
                message.data().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            buffer.put(message);
            Thread.sleep(1000);
        }
        System.out.println("Writer Stopped");
    }

    public void stop() {
        runFlag = false;
    }
}
