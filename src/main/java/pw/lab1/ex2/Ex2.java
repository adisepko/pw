package pw.lab1.ex2;

import io.vavr.control.Try;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Ex2 {

    private record SynchronizedFill(int[] array) {

        public void fillArray(int value) {
            IntStream.range(0, array.length)
                .forEach(i -> {
                    array[i] = value;
                    System.out.printf("Thread: %s, index: %d, value: %d%n", Thread.currentThread().getId(), i, value);
                    release();
                    lock();
                });
        }

        public void reverseFillArray(int value) {
            IntStream.range(0, array.length)
                .map(i -> array.length - i - 1)
                .forEach(i -> {
                    array[i] = value;
                    System.out.printf("Thread: %s, index: %d, value: %d%n", Thread.currentThread().getId(), i, value);
                    release();
                    lock();
                });
        }

        public synchronized void lock() {
            Try.run(this::wait).get();
        }

        public synchronized void release() {
            Try.run(this::notifyAll).get();
        }

    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Podaj wielkość tablicy");
        Scanner scanner = new Scanner(System.in);
        int arraySize = Integer.parseInt(scanner.nextLine());
        int[] array = new int[arraySize];
        SynchronizedFill synchronizedFill = new SynchronizedFill(array);
        Thread t1 = new Thread(() -> {
            synchronizedFill.fillArray(2);
            synchronizedFill.release();
        });
        Thread t2 = new Thread(() -> {
            synchronizedFill.reverseFillArray(1);
            synchronizedFill.release();
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(Arrays.toString(array));
    }
}
