package pw.lab1.ex1;

import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class Ex1 {

    public static Ex1 newEx1() {
        return new Ex1();
    }

    public void runEx1(int first, int second) {
        FutureTask<Integer> firstTask = createDoubleTheXFutureTask(first);
        FutureTask<Integer> secondTask = createDoubleTheXFutureTask(second);
        runTaskInNewThread(firstTask);
        runTaskInNewThread(secondTask);
    }

    private FutureTask<Integer> createDoubleTheXFutureTask(int x) {
        return new FutureTask<>(new DoubleTheX(x));
    }

    private void runTaskInNewThread(FutureTask<Integer> task) {
        Thread thread = new Thread(task);
        thread.start();

    }

    record DoubleTheX(int x) implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            int doubled = 2 * x;
            System.out.printf("Thread: %s, passed value: %d, doubled value: %d%n", Thread.currentThread().getId(), x, doubled);
            return doubled;
        }
    }

    public static void main(String[] args) {
        System.out.println("Enter first integer");
        Scanner scanner = new Scanner(System.in);
        int first = Integer.parseInt(scanner.nextLine());
        System.out.println("Enter second integer");
        int second = Integer.parseInt(scanner.nextLine());
        Ex1.newEx1().runEx1(first, second);
    }
}
