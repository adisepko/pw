package pw.lab1.ex3;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class Writer implements Runnable {

    private Data data;

    @SneakyThrows
    public void run() {
        String[] packets = {
            "First packet",
            "Second packet",
            "Third packet",
            "Fourth packet",
            "End"
        };

        for (String packet : packets) {
            System.out.printf("Thread: %s, Wrote: %s%n", Thread.currentThread().getId(), packet);
            data.send(packet);
            Thread.sleep(100);
        }
    }
}