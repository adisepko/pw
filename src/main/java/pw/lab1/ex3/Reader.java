package pw.lab1.ex3;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class Reader implements Runnable {

    private Data load;

    @SneakyThrows
    public void run() {
        for (String receivedMessage = load.receive();
            !"End".equals(receivedMessage);
            receivedMessage = load.receive()) {

            System.out.printf("Thread: %s, Read: %s%n", Thread.currentThread().getId(), receivedMessage);
            Thread.sleep(100);

        }
    }
}