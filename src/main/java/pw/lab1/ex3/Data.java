package pw.lab1.ex3;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Data {

    private String packet;

    private boolean transfer = true;

    public synchronized String receive() {
        while (transfer) {
            try {
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                log.error("Thread Interrupted");
            }
        }
        transfer = true;

        notifyAll();
        return packet;
    }

    public synchronized void send(String packet) {
        while (!transfer) {
            try {
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                log.error("Thread Interrupted");
            }
        }
        transfer = false;

        this.packet = packet;
        notifyAll();
    }
}