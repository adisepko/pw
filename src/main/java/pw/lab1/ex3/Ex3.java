package pw.lab1.ex3;

public class Ex3 {

    public static void main(String[] args) {
        Data data = new Data();
        Thread sender = new Thread(new Writer(data));
        Thread receiver = new Thread(new Reader(data));

        sender.start();
        receiver.start();
    }

}
